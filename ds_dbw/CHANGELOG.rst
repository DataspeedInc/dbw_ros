^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ds_dbw
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.3.2 (2025-02-18)
------------------

2.3.1 (2024-12-18)
------------------

2.3.0 (2024-11-12)
------------------

2.2.3 (2024-09-30)
------------------

2.2.2 (2024-09-09)
------------------

2.2.1 (2024-08-12)
------------------

2.2.0 (2024-07-17)
------------------
* Remove some settings changes in ROS install script
* Better desktop startup launcher with ROS environment already sourced in bashrc
* Add support for Ubuntu Noble with ROS Jazzy in ROS install script
* Contributors: Gabe, Kevin Hallenbeck

2.1.16 (2024-06-17)
-------------------

2.1.15 (2024-06-07)
-------------------

2.1.14 (2024-05-29)
-------------------

2.1.13 (2024-05-13)
-------------------

2.1.12 (2024-04-01)
-------------------

2.1.11 (2024-03-05)
-------------------

2.1.10 (2024-02-27)
-------------------

2.1.9 (2024-02-23)
------------------

2.1.8 (2024-02-20)
------------------

2.1.7 (2024-02-12)
------------------

2.1.6 (2024-01-16)
------------------

2.1.5 (2024-01-03)
------------------
* Merge DBW1/DBW2 README
* Use main branch for DBW2 install scripts
* Contributors: Kevin Hallenbeck

2.1.4 (2023-12-13)
------------------
* Single package for all platforms with new DBW2 CAN API
* Contributors: Kevin Hallenbeck
