^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ds_dbw_joystick_demo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.3.2 (2025-02-18)
------------------
* Add AEB precharge control to brake command and joystick demo
* Contributors: Kevin Hallenbeck

2.3.1 (2024-12-18)
------------------

2.3.0 (2024-11-12)
------------------
* Remove deprecated turn signal from MiscReport and MiscCmd
* Contributors: Kevin Hallenbeck

2.2.3 (2024-09-30)
------------------

2.2.2 (2024-09-09)
------------------

2.2.1 (2024-08-12)
------------------

2.2.0 (2024-07-17)
------------------
* Separate turn signal messages with diagnostics
  Keep functionality in misc cmd/report for a while to ease the transition
* Contributors: Kevin Hallenbeck

2.1.16 (2024-06-17)
-------------------

2.1.15 (2024-06-07)
-------------------

2.1.14 (2024-05-29)
-------------------

2.1.13 (2024-05-13)
-------------------

2.1.12 (2024-04-01)
-------------------

2.1.11 (2024-03-05)
-------------------
* Use system enable/disable buttons when mode-sync is inactive
* Contributors: Kevin Hallenbeck

2.1.10 (2024-02-27)
-------------------
* Add missing rclcpp_components dependency to package.xml
* Contributors: Kevin Hallenbeck

2.1.9 (2024-02-23)
------------------

2.1.8 (2024-02-20)
------------------

2.1.7 (2024-02-12)
------------------
* Add more documentation for DBW2 game-controller joystick-demo
* Steering wheel angle as degrees instead of radians
* Contributors: Kevin Hallenbeck

2.1.6 (2024-01-16)
------------------
* Refactor
* Fix joy device_id
* Contributors: Kevin Hallenbeck

2.1.5 (2024-01-03)
------------------

2.1.4 (2023-12-13)
------------------
* Single package for all platforms with new DBW2 CAN API
* Contributors: Kevin Hallenbeck
